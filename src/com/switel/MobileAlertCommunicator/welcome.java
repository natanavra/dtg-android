package com.switel.MobileAlertCommunicator;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import static com.switel.MobileAlertCommunicator.Languageset.LANGUAGE_TYPE;

public class welcome extends Activity{

	private static final int REQUEST_CODE_SETTING = 1001;

	Timer tr;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_CONTEXT_MENU);
		setContentView(R.layout.welcome);

		String language=SharePreferenceUtil.getPreference(this, LANGUAGE_TYPE);
		Log.e("language", language);
		Locale.setDefault(Locale.ENGLISH);
		if(language != null && language.length() > 0) setLanguage(Integer.parseInt(language));
		else setLanguage(0);

		tr = new Timer();
		tr.schedule(new TimerTask() {
			@Override
			public void run() {
				this.cancel();
				Intent intent = new Intent(welcome.this,MainMenuSetting.class);
				startActivity(intent);
				finish();
			}
		}, 1000);

	}

	private void setLanguage(int language){
		Locale loc;
		switch(language){
			default:
			case Languageset.ENGLISH:
				Locale.setDefault(Locale.ENGLISH);
				loc = Locale.getDefault();
				break;

			case Languageset.DUTCH:
				loc = Locale.JAPAN;
				break;

			case Languageset.FRENCH:
				loc = Locale.FRENCH;
				break;

			case Languageset.GERMAN:
				loc = Locale.GERMAN;
				break;

			case Languageset.PORTUGUESE:
				loc = new Locale("pt");
				break;

			case Languageset.SPANISH:
				loc = Locale.CHINESE;
				break;

			case Languageset.TURKISH:
				loc = Locale.KOREAN;
				break;

			case Languageset.ITALIAN:
				loc = Locale.ITALIAN;
				break;

			case Languageset.HEBREW:
				loc = new Locale("he");
				break;
		}

		Configuration config = getResources().getConfiguration();
		if(config.locale != loc) {
			SharedPreferences uiState = getSharedPreferences("system", Activity.MODE_PRIVATE);
			SharedPreferences.Editor edit = uiState.edit();
			edit.putString(LANGUAGE_TYPE, language + "");
			edit.commit();

			config.locale = loc;
			getResources().updateConfiguration(config, getResources().getDisplayMetrics());//更新配置
			finish();
			Intent intent = new Intent(welcome.this, welcome.class);
			startActivity(intent);
		}
	}

}
